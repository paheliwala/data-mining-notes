dm.dvi : dm.tex
	latex dm.tex
	bibtex8 dm
	makeindex dm
	latex dm
	latex dm

dm.pdf : dm.tex
	pdflatex dm.tex
	bibtex8 dm
	makeindex dm
	pdflatex dm.tex
	pdflatex dm.tex

clean:
	-rm -f *.pdf *.dvi
	-rm -f *.log
	-rm -f *.toc
	-rm -f *.aux dm.bbl dm.blg dm.idx dm.ilg dm.ind
