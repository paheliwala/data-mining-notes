// Sigmoid function
// TODO
import graph;
size(6cm, IgnoreAspect);
real f(real x){return 1/(1+exp(-x));}
xlimits( -5, 5 );
ylimits( 0, 1.2 );
draw(graph(f,-5,5));
xaxis(Label("$z$",position=EndPoint, align=NE),Bottom,LeftTicks(NoZero));
yaxis("y");

dot((0,0));
