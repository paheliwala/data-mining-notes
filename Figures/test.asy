size(0,100);

path unitcirtle=E..N..W..S..cycle;


/* Neuron */
draw(scale(4)*unitcirtle);
dot((0,0));
dot((-4,0));
dot("$\theta x$",(0,0),N);

/* The inputs */
draw((-12,8)--(-4,0));
draw((-12,0)--(-4,0));
draw((-12,-8)--(-4,0));
draw("$x_1$",(-12,8),W);
draw("$x_2$",(-12,0),W);
draw("$x_3$",(-12,-8),W);
/* Output */
draw((4,0)--(12,0));
draw("$a=g(z)$",(12,0),E);
