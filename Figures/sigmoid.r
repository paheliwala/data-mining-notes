#

sigmoid <- function(z) { 1/(1+exp(-z)) }
xx <- seq(from=-10,to=10,by=0.1)
yy <- sigmoid( xx )
pdf(file='sigmoid.pdf',height=3,width=3)
plot(xx,yy,type='l',xlab='z',ylab='sigmoid(z)')
dev.off()
