size(0,300);


picture neuron( int i, int l ) {
    picture p1;
    draw( p1, scale(1)*unitcircle );
 //   dot( p1, (0,0) );
    dot( p1, (-1,0) );
    if ( l == 0 ) {
        label( p1, format( "$x_%d$", i ), 0);
    } else if ( l == 3 ) {
        label( p1, format( "$y_%d$", i ), 0);
    } else { 
        label( p1, format( "$a_%d", i ) +
                format( "^{(%d)}$", l )
                , 0);
    }
//    label( p1, "$\theta x$",0);
    return p1;
}

for ( int i = 0; i < 4; ++i ) {
    real h_spacing = 5.0;
    add( shift(0*h_spacing,3*i)*neuron( i+1, 0 ) );
    add( shift(1*h_spacing,3*i)*neuron( i+1, 1 ) );
    add( shift(2*h_spacing,3*i)*neuron( i+1, 2 ) );
    add( shift(3*h_spacing,3*i)*neuron( i+1, 3 ) );
    for ( int j = 0; j < 4; ++j ) {
        draw( (0*h_spacing+1,3*i)--(1*h_spacing-1,3*j) );
        draw( (1*h_spacing+1,3*i)--(2*h_spacing-1,3*j) );
        draw( (2*h_spacing+1,3*i)--(3*h_spacing-1,3*j) );
    }
}
